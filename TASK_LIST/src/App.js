import { BrowserRouter, Switch, Route} from 'react-router-dom';

import "react-datepicker/dist/react-datepicker.css";

import Home from './pages/Home'
import Details from './pages/Details'

import './styles.css'

const Routes = () => {
  return(
    <BrowserRouter>
      <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/details/:id" component={Details} />
      </Switch>
    </BrowserRouter>
  )
}

export default Routes;