import DatePicker from "react-datepicker";

import axios from 'axios';

import { Link } from "react-router-dom";

import { FaCheck } from "react-icons/fa";

import { useEffect, useState } from "react";

import moment from 'moment';

import "./styles.css";

const Home = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");
  //pondo os dados em um array
  const [tasksBD, setTasksBD] = useState([]);

  //Função que executa a mutação do resultado da variável
  useEffect(() => {
    getTasks();
  }, []);

  //Update registro
  const updateTask = async (id, status) => {
    await axios.put('http://localhost:3003/todo/' + id, {
      status: !status,
    })
    // alert('Atualizado!'+id);
    getTasks();

  }

  //Salvar task
  const saveTask = async () => {
    await axios.post('http://localhost:3003/todo',{
      title,
      description,
      date
    });
    //Cerrega as tasks - para fazer o reload dos dados que vêm do servidor
    getTasks();
    //Exibe mensagem de salvo
    alert('Salvo com sucesso!');
  }

  //Listar tasks
  const getTasks = async () => {
    const tasks = await axios.get('http://localhost:3003/todo');
    // console.log("TASK:", tasks.data);
    setTasksBD(tasks.data);
  }

  return (
    <div className="container-home">
      <div className="subcontainer-home">
        <div className="subcontainer-left">
          <h1>Task List</h1>
          <p>
            Junte-se a mais de meio milhão de usuários e gerencie sua rotina da
            melhor forma.
          </p>

          <div className="container-form">
            <input
              placeholder="Título"
              onChange={(txt) => setTitle(txt.target.value)}
            />
            <textarea
              placeholder="Descrição"
              onChange={(txt) => setDescription(txt.target.value)}
            />
            <DatePicker
              dateFormat="d/M/yyyy"
              selected={date}
              onChange={(txt) => setDate(txt)}
            />
            <button className="btn-save" onClick={saveTask}>Salvar</button>
          </div>
        </div>

        <ul className="subcontainer-right">

          {tasksBD.map(item => {

            const formattedDate = moment(item.date).format('DD/MM/yyyy');

            return(
              <li key={item._id}>
                <div>
                
                  <Link to={"/details/" + item._id}>
                      <h2 style={ item.status ? {} : {textDecoration: 'line-through'}}>{item.title}</h2>
                      <h3>{formattedDate}</h3>
                      <h3>{item.description}</h3>
                      <p>Status: {item.status ? 'A FAZER' : 'FEITO'}</p>
                  </Link>

                </div>                  
                <button onClick={ () => updateTask(item._id, item.status)}>
                  <FaCheck size={22} color="#1a1a1a" />
                </button>
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  );
};

export default Home;
