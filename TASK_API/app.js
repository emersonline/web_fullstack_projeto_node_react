const express = require('express');
const mongoose = require('mongoose');
//biblioteca que define que tem algum usuário que consegue acessar
const cors = require('cors');

const ToDoSchema = require('./schemas/ToDoSchema');

const server = express();

//para algum tipo de cliente acessar, colocoria como parâmetro o endereço dele
//vazio = indica que qualquer pessoa pode acessar
server.use(cors());

server.use(express.json());

mongoose.connect('mongodb+srv://admin:admin@cluster0.xmg7m1u.mongodb.net/posweb?retryWrites=true&w=majority',{
    useNewUrlParser: true,
    useUnifiedTopology: true 
});

//rota listagem
server.get('/todo', async (req, res) => {
    // return res.json({message: 'Seja bem-vindo a API do TODO_LIST'});
    const todo = await ToDoSchema.find();

    return res.status(201).json(todo);
});

// //Pegando registro por dados específicos
// server.get('/aluno/:id', async(request, response) => {
//     const { id } = request.params;

//     const aluno = await AlunoSchema.findById(id);

//     return response.status(200).json(aluno); 
// });
//rota pegar um registro
server.get('/todo/:id', async (req, res) => {
    // return res.json({message: 'Seja bem-vindo a API do TODO_LIST'});
    const { id } = req.params;

    const todo = await ToDoSchema.findById(id);

    return res.status(200).json(todo);
});

//Rota para inserção  
server.post('/todo', async (req, res) => {
    const { title, date } = req.body;

    if(!title || !date){
        return res.status(400).json({message: 'Validations Fails'});
    }
    const todo = await ToDoSchema.create(req.body);
    return res.json(todo);
});

// Rota de atualização de registro
server.put('/todo/:id', async (req, res) => {
    const { id } = req.params;
    const todo = await ToDoSchema.findOneAndUpdate({'_id':id}, req.body);
    return res.json(todo); 
})

//DELETANDO
server.delete('/todo/:id', async(req, res) => {
    const { id } = req.params;
    const todo = await ToDoSchema.deleteOne({'_id':id});
    return res.json({message: 'Successfully deleted'}); 
});

server.listen(3003, () => console.log('Servidor iniciado na porta http://localhost:3003'));
