import DatePicker from 'react-datepicker';

import { FaArrowLeft } from 'react-icons/fa';

import { Link, useParams } from 'react-router-dom';

import { useState, useEffect } from 'react';

import axios from 'axios';

import { ToastContainer, toast} from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css'

import './styles.css';

const Details = ({ history }) => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [date, setDate] = useState('');

    const params = useParams();
    const id = params.id;

    useEffect(() => {
        getTask();
    }, [])

    const notify = () => toast('Atualizado com sucesso!!', {type: 'success'});

    const getTask = async () => {
        const task = await axios.get('http://localhost:3003/todo/' + id);
        // console.log(task.data.title);
        setTitle(task.data.title);
        setDescription(task.data.description);
        
        const formmatedDate = new Date(task.data.date);
        setDate(formmatedDate);
    }

    //Excluir task
    const removeTask = async () => {
        await axios.delete('http://localhost:3003/todo/' + id);

        notify();

        history.push('/');
    }
   
    //Update task
    const updateTask = async () => {
        await axios.put('http://localhost:3003/todo/' + id, {
            title,
            description,
            date
        })

        // history.push('/')
        notify();
    }

    return (
        <div className='container-details'>
            <ToastContainer/>
            <div className='subcontainer-details'>

                <div className='container-header'>
                    <Link to="/">
                        <FaArrowLeft />
                        <span>Voltar</span>
                    </Link>
                </div>

                <input value={title} placeholder="Título" onChange={(txt) => setTitle(txt.target.value)} />
                <textarea value={description} placeholder="Descrição" onChange={(txt) => setDescription(txt.target.value)} />
                <DatePicker value={date} dateFormat="d/M/yyyy" selected={date} onChange={(txt) => setDate(txt)} />

                <div className='container-buttons'>
                    <button onClick={updateTask}>Salvar</button>
                    <button onClick={removeTask}>Excluir</button>
                </div>
            </div>
        </div>
    )
}

export default Details;